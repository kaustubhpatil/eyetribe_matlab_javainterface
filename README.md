This is a simple Java thread interface to the eyetribe SDK.

For more sophisticated implementation see: https://github.com/esdalmaijer/EyeTribe-Toolbox-for-Matlab

Follow these steps (at your own risk).

1. Make MATALAB use latest Java: on windows machine set MATLAB_JAVA environmental variable to corresponding jdk/jre. I am using MATLAB R2014b with Java 1.8.0_25-b18. Restart MATLAB. Check java version in MATLAb using "version - java".

2. Download and compile the TETThread.java file: 

    javac -cp TETJavaClient.jar TETThread.java

    This should create a TETThread.class file. You need to get the jar file from the eyetribe.

3. In MATLAB add the java path:

    javaaddpath 'path_to_TETThread.class' (path to the folder)

    javaaddpath  'path_to_TETJavaClient.jar' (including the file name)

4. In MATLAB create thread and record data to a file:

    x = TETThread('tet_data.txt', 'test');

    x.begin('starting to record')

    x.end('stopping')

This will create a file 'tet_data.txt' with data in following format:

[timestamp, smoothed_x_coord, smoothed_y_coord, isFixed]