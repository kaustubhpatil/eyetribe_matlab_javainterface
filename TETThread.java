import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;

import com.theeyetribe.client.GazeManager;
import com.theeyetribe.client.GazeManager.ApiVersion;
import com.theeyetribe.client.GazeManager.ClientMode;
import com.theeyetribe.client.IGazeListener;
import com.theeyetribe.client.data.GazeData;

public class TETThread extends Thread
{			
	GazeManager gm;
	GazeListener gazeListener;
	String filename;
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	Date date = new Date();
	BufferedWriter output;
	static LinkedList<double[]> data; //collect data in this
	public static boolean record; 
	private boolean active;	//is the device active
	
	public TETThread(String filename, String msg) {
		gm =  null;
		gazeListener = null;
		this.filename = filename;
		data = new LinkedList<double[]>();
		record = true;
		try {        	
        	output = new BufferedWriter(new FileWriter(filename));
        	output.write(msg + " " + dateFormat.format(date) + "\n");
		}
		catch (IOException e) {
			System.err.println("Error: " + e.getMessage());
		}
	}
	
	//don't call this directly if you want any messages incorporated
    @Override
    public void run()
    {
    	gm = GazeManager.getInstance();
		active = gm.activate(ApiVersion.VERSION_1_0, ClientMode.PUSH);
		System.out.println("active: " + active);
    	gazeListener  = new GazeListener();
    	gm.addGazeListener((IGazeListener) gazeListener);    	
    }
	
    public void begin() {
    	begin("begin");
    }
    
	public void begin(String msg)	
	{
		try {
        	output.write(msg + " " + dateFormat.format(date) + "\n"); 
    	}
    	catch (IOException e) {
			System.err.println("Error: " + e.getMessage());
		}
		run();
	}
    
	public void end() {
    	end("end");
    }
	
    public void end(String msg)
    {
    	gm.removeGazeListener(gazeListener);
        gm.deactivate();
        //write to file
        try {
        	for (int i = 0; i < data.size(); i++) {
        	    double[] txy = data.get(i);
        	    output.write(Arrays.toString(txy) + "\n");        	    
        	}
        	output.write(msg + " " + dateFormat.format(date));
        	output.close();
		}
		catch (IOException e) {
			System.err.println("Error: " + e.getMessage());
		}
    }
    
    private static class GazeListener implements IGazeListener
    {
        @Override
        public void onGazeUpdate(GazeData gazeData)
        {
            //System.out.println(gazeData.toString());
        	if(record) {
        		double[] xy = new double[4];
        		xy[0] = (double) gazeData.timeStamp;
        		xy[1] = gazeData.smoothedCoordinates.x;
        		xy[2] = gazeData.smoothedCoordinates.y;
        		double isFixated = (gazeData.isFixated) ? 1.0 : 0.0;
        		xy[3] = isFixated;        	
        		data.add(xy);
        	}
            //System.out.println(xCoord);         
            //System.out.println(yCoord);
            //System.out.println(gazeData.isFixated);
        }
    }
}
